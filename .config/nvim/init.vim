"==========================
"   GENERAL
"==========================
set nocompatible            " use vim settings rather than vi
set history=1000            " store up to one thousand commands on history
set autoread                " reload file changes outside vim
set mouse-=a                " disable mouse click to position
set hidden                  " allow vim buffers to exist in background
set timeoutlen=300          " quick timeouts on key combinations
set clipboard+=unnamedplus  " use the system clipboard (install 'xsel' to manage clipboards)

"==========================
"   PLUGINS
"==========================
call plug#begin('~/.config/nvim/plugged')

"==============
"====THEMES====
"==============

" Flattened theme
Plug 'romainl/flattened'

" Color schemes
Plug 'flazz/vim-colorschemes'

"==============
"===PLUGINS====
"==============

" Status line
Plug 'vim-airline/vim-airline'

" Git tools (:Gstatus, etc)
Plug 'tpope/vim-fugitive'

" Nerd tree (mapped to Ctrl + N)
Plug 'scrooloose/nerdtree'

" Auto pair [({
Plug 'jiangmiao/auto-pairs'

" Auto close tags after >
Plug 'alvan/vim-closetag'

" Make comments easily (gc<motion>, eg: gc7j)
Plug 'vim-scripts/tComment'

" % also matches HTML tags / words / etc
Plug 'vim-scripts/matchit.zip'

" <Tab> completions
Plug 'ervandew/supertab'

" Sync color scheme with tmux
Plug 'edkolev/tmuxline.vim'

call plug#end()

"==========================
"   INDENTATION
"==========================
set autoindent              " auto indent the lines
set smartindent             " smart indentation
set smarttab                " smart tab indentation
set tabstop=4               " number of visual spaces per tab
set softtabstop=4           " number of tabs when editing
set shiftwidth=4            " number of spaces for indentation
set expandtab               " convert tab into spaces
syntax on
filetype on
filetype plugin indent on    " enable indentation by plugins

"==========================
"   DISPLAY
"==========================
set lazyredraw              " redraw only when needed
set title                   " display file name in the terminal's title
set linebreak               " wrap lines at convenient places
set showmode                " show current mode on the bottom
set showcmd                 " show incomplete commands on the bottom
set showmatch               " higlight matching [{()}]
set ruler                   " current line and percentage on the bottom
set relativenumber          " show numbers relative to current line
set number                  " show current line number
set visualbell              " visual bell instead of sound
set list                    " enable list characters
set listchars=tab:▸\ ,trail:¬,nbsp:.,extends:❯,precedes:❮ " show trailing whitespaces

"==========================
"   FOLDING
"==========================
set foldmethod=indent       " fold based on indentation
set foldnestmax=3           " deepest fold is three levels
set nofoldenable            " dont fold by default

"==========================
"   COMPLETION
"==========================
set wildmenu                " enable auto-complete with tab
set wildmode=full           " auto-complete with first occurrence

"==========================
"   SCROLLING
"==========================
set scrolloff=10            " scroll window when ten lines away from margins

"==========================
"   SEARCHING
"==========================
set incsearch               " search as characters are entered
set hlsearch                " search highlight
set ignorecase              " ignore case when searching
set smartcase               " dont ignore case if capital is typed

"==========================
"   COLORS
"==========================
syntax on                   " enable syntax highlightning
set t_Co=256                " set colors
set background=dark         " set background dark
colorscheme flattened_dark  " color scheme

"==========================
"   AUTO COMMANDS
"==========================
au BufNewFile,BufRead *.lsx set filetype=xml " open all .lsx files as xml
au BufNewFile,BufRead *.pl set filetype=prolog " open all .pl files as prolog
au BufNewFile,BufRead *.gradle set filetype=groovy " open all .gradle files as groovy
au BufNewFile,BufRead *.*rc set filetype=sh " open all .*rc files as shell script
au FocusLost,WinLeave * :silent! wa " save all opened files when vim loses focus
au VimResized * :wincmd = " update vim size when window is resized
au BufEnter * silent! cd %:p:h " change working directory to file's dir
au BufEnter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif " close nerd vim if only thing opened is nerd tree

"==========================
"   MACROS
"==========================
map <Esc> :nohlsearch<CR>
map <C-n> :NERDTreeToggle<CR> cd r
map <C-p> :call PreviewMarkdown()<CR>
nnoremap j gj
nnoremap k gk
vnoremap j gj
vnoremap k gk
nnoremap <Down> gj
nnoremap <Up> gk
vnoremap <Down> gj
vnoremap <Up> gk
inoremap <Down> <C-o>gj
inoremap <Up> <C-o>gk

"==========================
"   ASSIGN VARIABLES
"==========================
let g:closetag_filenames = "*.html,*.xhtml,*.phtml,*.php,*.xml,*.lsx"
let g:airline_powerline_fonts = 1
let g:airline_theme='badwolf'

"==========================
"   CUSTOM FUNCTIONS
"==========================

" Preview markdown in Firefox
function! PreviewMarkdown()
  let outFile ='/tmp/''%:r''.html'
  silent execute '!markdown ''%'' > ' . outFile
  silent execute '!firefox ' . outFile
endfunction
