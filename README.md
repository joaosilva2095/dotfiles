Linux - DotFiles
================

A compilation of all dot files used by myself while coding.
Right now I have added ZSH, TMux and NeoVim configuration files.

*This is still under construction though*

Screenshots
===========

### ZSH Terminal

![ZSH Terminal](https://i.imgur.com/TOTvkOV.png)

### NeoVim
![NeoVim](https://i.imgur.com/8hUf7sK.png)

### TMux

![Tmux](https://i.imgur.com/mlLz8nu.png)

### NeoVim and Tmux

![NeoVim and TMux](https://i.imgur.com/5NUm1sT.png)

Installation
============

First clone the project with
```
$ git clone https://gitlab.com/joaosilva2095/dotfiles.git
```

Add executable permissions to the installation script
```
$ chmod +x install.sh
```

After that simply run the bash script, this will create symbolic links to the configurations files
```
$ ./install.sh
```
