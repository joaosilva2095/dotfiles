#!/bin/bash
CURR_DIR="`pwd`"                        # current working directory
CONF_DIR="$HOME/.config"                # configuration directory
FONT_DIR="$HOME/.local/share/fonts"     # fonts directory

# .tmux.conf
ln -f -s "$CURR_DIR/.tmux.conf" "$HOME/"

# .zshrc
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
rm ~/.zshrc
ln -f -s "$CURR_DIR/.zshrc" "$HOME/"
. "$HOME/.zshrc"

# .config
mkdir -p "$CONF_DIR"
for f in "$CURR_DIR"/.config/*; do
    ln -f -s "$f" "$CONF_DIR"
done
nvim +PlugInstall

# .fonts
mkdir -p "$FONT_DIR"
fc-cache -f "$FONT_DIR"
for f in "$CURR_DIR"/.fonts/*; do
    ln -f -s "$f" "$FONT_DIR"
done
